import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Note } from "../interfaces";

interface NotesState {
  notes: Note[];
}
//function to get notes from local Storage
const loadNotesFromLocalStorage = (): NotesState => {
  try {
    const storedNotes = localStorage.getItem("savedNotes");
    if (storedNotes) {
      return { notes: JSON.parse(storedNotes) };
    }
  } catch (error) {
    console.error("Error loading notes from local storage:", error);
  }

  return {
    notes: [],
  };
};
//initiate the notes
const initialState: NotesState = loadNotesFromLocalStorage();

const noteSlice = createSlice({
  name: "notes",
  initialState,
  reducers: {
    add: (state, action: PayloadAction<Note>) => {
      state.notes.push(action.payload);
    },
    update: (state, action: PayloadAction<Note>) => {
      const index = state.notes.findIndex(
        (note) => note.id === action.payload.id
      );
      if (index !== -1) state.notes[index] = action.payload;
    },
    deleteNote: (state, action: PayloadAction<string>) => {
      state.notes = state.notes.filter((note) => note.id !== action.payload);
    },
  },
});

export const { add, update, deleteNote } = noteSlice.actions;

export default noteSlice.reducer;
