export interface Note {
  id: string;
  title?: string;
  description?: string;
}
export interface FormNoteProps {
  formData: Note;
  setFormData: React.Dispatch<React.SetStateAction<Note>>;
}
export interface NoteCardProps {
  note: Note;
  openModal: Function;
  setCurrentNote: React.Dispatch<React.SetStateAction<Note>>;
}
