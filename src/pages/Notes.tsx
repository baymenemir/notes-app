import { useDispatch, useSelector } from "react-redux";
import { RootState, AppDispatch } from "../redux/Store";
import { ChangeEvent, useEffect, useState } from "react";
import { update as updateNoteAction } from "../redux/Slice";
import Modal from "react-modal";
import { Note } from "../interfaces";
import FormNote from "../components/FormNote";
import NoteCard from "../components/NoteCard";
import "./Notes.css";
function NotesPage() {
  //variables
  const dispatch = useDispatch<AppDispatch>(); // useDispatch with typed AppDispatch
  //the notes variable
  const notes = useSelector((state: RootState) => state.notes.notes);
  //variable representing the values in the form
  const [formData, setFormData] = useState<Note>({
    id: "0",
    title: "",
    description: "",
  });
  const [modalIsOpen, setIsOpen] = useState(false);
  //the note selected to be updated
  const [currentNote, setCurrentNote] = useState<Note>({
    id: "0",
    title: "",
    description: "",
  });
  //styling for modal
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
    },
  };

  //functions
  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  //function used to update the variable representing the note to be updated
  const handleInputChangeUpdate = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    console.log(name + " , " + value);
    const currentNoteUpdate: Note = {
      ...currentNote,
      [name]: value,
    };
    setCurrentNote(currentNoteUpdate);
  };

  const updateNote = (note: Note) => {
    dispatch(updateNoteAction(note));
    closeModal();
  };
  // function that saves a note that hasn't been finnished when exiting the website
  useEffect(() => {
    const handleBeforeUnload = (event: BeforeUnloadEvent) => {
      event.preventDefault();

      if (formData.title === "" || formData.description === "") {
        localStorage.setItem(
          "savedNotes",
          JSON.stringify([...notes, formData])
        );
      }
      event.returnValue = "";
    };

    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, [formData]);
  // function that saves all available notes when exiting the website
  useEffect(() => {
    const handleBeforeUnload = (event: BeforeUnloadEvent) => {
      event.preventDefault();
      localStorage.setItem("savedNotes", JSON.stringify(notes));

      event.returnValue = "";
    };

    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, [notes]);

  return (
    <div className="container">
      <div>
        <FormNote formData={formData} setFormData={setFormData}></FormNote>
      </div>
      <div className="notes_list">
        {notes ? (
          notes
            .slice()
            .reverse()
            .map((note: Note) => (
              <NoteCard
                note={note}
                openModal={openModal}
                setCurrentNote={setCurrentNote}
              ></NoteCard>
            ))
        ) : (
          <></>
        )}
      </div>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <table>
          <tbody>
            <tr hidden>
              {" "}
              <td>
                {" "}
                <input
                  type="text"
                  defaultValue={currentNote ? currentNote.id : ""}
                />
              </td>
            </tr>
            <tr>
              <td>
                <h2>Title:</h2>
              </td>
              <td>
                <input
                  type="text"
                  name="title"
                  value={currentNote ? currentNote.title : ""}
                  onChange={handleInputChangeUpdate}
                />
              </td>
            </tr>
            <tr>
              <td>
                <h2>Description:</h2>
              </td>
              <td>
                <input
                  type="text"
                  name="description"
                  value={currentNote ? currentNote.description : ""}
                  onChange={handleInputChangeUpdate}
                />
              </td>
              <td>
                <button
                  type="submit"
                  onClick={() => {
                    updateNote(currentNote);
                  }}
                >
                  {" "}
                  update
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </Modal>
    </div>
  );
}

export default NotesPage;
