import { useDispatch } from "react-redux";
import { FormNoteProps } from "../interfaces";
import { add as addNoteAction } from "../redux/Slice";
import { v4 as uuidv4 } from "uuid";
import { AppDispatch } from "../redux/Store";
import { ChangeEvent } from "react";
const FormNote: React.FC<FormNoteProps> = ({ formData, setFormData }) => {
  const dispatch = useDispatch<AppDispatch>();
    //update the variabl representing the values in the form
  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    console.log(name + " , " + value);
    setFormData({
      ...formData,
      [name]: value,
    });
  };
  const addNote = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    const id = uuidv4();
    if (formData.title !== "" || formData.description !== "")
      dispatch(addNoteAction({ ...formData, id: id }));
    setFormData({ id: "", title: "", description: "" });
  };

  return (
    <table>
      <tbody>
        <tr>
          <td>
            <h2>Title:</h2>
          </td>
          <td>
            <input
              type="text"
              name="title"
              value={formData.title}
              onChange={handleInputChange}
            />
          </td>
        </tr>
        <tr>
          <td>
            <h2>Description:</h2>
          </td>
          <td>
            <input
              type="text"
              name="description"
              value={formData.description}
              onChange={handleInputChange}
            />
          </td>
          <td>
            <button type="submit" onClick={addNote}>
              {" "}
              add
            </button>
          </td>
        </tr>
      </tbody>
    </table>
  );
};
export default FormNote;
