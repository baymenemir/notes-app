import { useDispatch } from "react-redux";
import { NoteCardProps } from "../interfaces";
import { deleteNote as deleteNoteAction } from "../redux/Slice";
import { AppDispatch } from "../redux/Store";
import "./NoteCard.css";
const NoteCard: React.FC<NoteCardProps> = ({
  note,
  setCurrentNote,
  openModal,
}) => {
  const dispatch = useDispatch<AppDispatch>();

  const deleteNote = (id: string) => {
    dispatch(deleteNoteAction(id));
  };

  return (
    <div className="card">
      <table
        key={note.id}
        style={{
          width: "100%",
          padding: "10px",
          margin: "10px",
        }}
      >
        <tbody>
          <tr>
            <td>
              <h1>Title : </h1>
            </td>
            <td>
              <h1>{note.title}</h1>
            </td>
          </tr>
          <tr>
            <td>
              <h3>Description : </h3>
            </td>
            <td>
              {" "}
              <h3>{note.description}</h3>
            </td>
          </tr>
          <tr>
            <td>
              <button
                onClick={() => {
                  deleteNote(note.id);
                }}
              >
                Delete
              </button>
            </td>
            <td>
              <button
                onClick={() => {
                  openModal();
                  setCurrentNote(note);
                }}
              >
                Update
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
export default NoteCard;
